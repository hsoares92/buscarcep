﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Busca_Cep.Services
{
    class SerializationService
    {
        public static T DeserializeObject<T>(string jsonString)
        {
            //DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            T result = JsonConvert.DeserializeObject<T>(jsonString);

            return result;
        }

        public static List<T> DeserializeArrayObject<T>(string jsonString)
        {
            //DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(T));
            List<T> result = JsonConvert.DeserializeObject<List<T>>(jsonString);

            return result;
        }
    }
}
